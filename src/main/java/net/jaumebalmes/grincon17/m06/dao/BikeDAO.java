package net.jaumebalmes.grincon17.m06.dao;

public interface BikeDAO {
    void getBikes();
    void addNewBike(String model, String plate);
    void deleteEntry(int id, String table);
}
