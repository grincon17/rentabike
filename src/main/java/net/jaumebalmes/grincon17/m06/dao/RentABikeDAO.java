package net.jaumebalmes.grincon17.m06.dao;

import java.sql.Date;

public interface RentABikeDAO {
    void getRents();
    void getRentsByDay(Date date);
    void addNewRent(int BikeId, int ClientId, Date start, Date end);
    void modifyRentDay(int id, Date start, Date end);
}
