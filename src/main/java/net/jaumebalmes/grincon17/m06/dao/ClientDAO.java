package net.jaumebalmes.grincon17.m06.dao;

import java.sql.SQLException;

public interface ClientDAO {
    void getClients();
    void addNewClient(String name, String surname, String dni);
    void deleteEntry(int id, String table);
    void updateClientName(int id, String newName);
    void getClientNameStartingWith(String startWith);

}
