package net.jaumebalmes.grincon17.m06.managers;

import net.jaumebalmes.grincon17.m06.dao.BikeDAO;
import net.jaumebalmes.grincon17.m06.dao.ClientDAO;
import net.jaumebalmes.grincon17.m06.dao.RentABikeDAO;

import java.sql.*;
import java.time.LocalDate;

public class DAOManager implements ClientDAO, BikeDAO, RentABikeDAO {
    private String table;
    private String query;
    private ConnectionManager connectionManager = new ConnectionManager();

    public void createUpdateOrDeleteEntry(Connection connection, String query) {
        try (Statement statement = connection.createStatement()){
            statement.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    private void readEntry(Connection connection, String table, String query)  {
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                if(table.equals("rent")) {
                    System.out.println(
                            "Alquiler: " +
                                    resultSet.getInt("id") + "\t" +
                                    resultSet.getInt("id_moto") + "\t" +
                                    resultSet.getInt("id_client") + "\t" +
                                    resultSet.getDate("starDate") + "\t" +
                                    resultSet.getDate("endDate")
                    );
                }
                if(table.equals("moto")) {
                    System.out.println(
                            "Moto: " +
                                    resultSet.getInt("id") + "\t" +
                                    resultSet.getString("model") + "\t" +
                                    resultSet.getString("plate")
                    );
                }
                if(table.equals("client")) {
                    System.out.println("Client: " + resultSet.getInt("id") + "\t" +
                            resultSet.getString("nom") + "\t" +
                            resultSet.getString("surname") + "\t" +
                            resultSet.getString("dni")
                    );
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteEntry(int id, String table) {
        query = "DELETE FROM" + table + "WHERE id=" + id;
        try (Connection connection = connectionManager.getConnection()){
            createUpdateOrDeleteEntry(connection, query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

//******************************************* implementación ClientDAO ***********************************************//

    @Override
    public void getClients() {
        table = "client";
        query = "SELECT * FROM" + table;
        try (Connection connection = connectionManager.getConnection()) {
            System.out.println("***** Clientes ******");
            readEntry(connection, table, query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addNewClient(String name, String surname, String dni) {
        query = "INSERT INTO client (nom, cognoms, dni) VALUES ('" + name + "','" + surname + "','" + dni + "')";
        try (Connection connection = connectionManager.getConnection()){
            createUpdateOrDeleteEntry(connection, query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateClientName(int id, String newName) {
        query = "UPDATE client SET nom='" + newName + "' WHERE id=" + id;
        try (Connection connection = connectionManager.getConnection()){
            createUpdateOrDeleteEntry(connection, query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getClientNameStartingWith(String startWith) {
        table = "client";
        query = "SELECT FROM client WHERE name=" + startWith;
        try (Connection connection = connectionManager.getConnection()) {
            System.out.println("***** Clientes que empiezan por " + startWith + "******");
            readEntry(connection, table, query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

//****************************************** Implementación BikeDAO **************************************************//


    @Override
    public void getBikes() {
        table = "moto";
        query = "SELECT * FROM" + table;
        try (Connection connection = connectionManager.getConnection()) {
            System.out.println("***** Motos ******");
            readEntry(connection, table, query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addNewBike(String model, String plate) {
        query = "INSERT INTO moto (model, plate) VALUES ('" + model + "','" + plate + "')";
        try (Connection connection = connectionManager.getConnection()){
            createUpdateOrDeleteEntry(connection, query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


//****************************************** Implementación RentABikeDAO *********************************************//



    @Override
    public void getRents() {
        table = "rent";
        query = "SELECT * FROM" + table;
        try (Connection connection = connectionManager.getConnection()) {
            System.out.println("***** Motos ******");
            readEntry(connection, table, query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getRentsByDay(Date date) {
        table = "rent";
        query = "SELECT FROM" + table + "WHERE startDate=" + date;
        try (Connection connection = connectionManager.getConnection()) {
            System.out.println("***** Motos ******");
            readEntry(connection, table, query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addNewRent(int bikeId, int clientId, Date start, Date end) {
        query = "INSERT INTO rent (id_moto, id_client, startDate, endDate) VALUES ('" + bikeId + "','" +clientId + "')";
        try (Connection connection = connectionManager.getConnection()){
            createUpdateOrDeleteEntry(connection, query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void modifyRentDay(int id, Date start, Date end) {
        
    }
}
