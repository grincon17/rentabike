package net.jaumebalmes.grincon17.m06.managers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionManager {

    public Connection getConnection() throws SQLException {
        Connection conn = null;
        Properties connectionProps = new Properties();
        connectionProps.put("user", "admin");
        connectionProps.put("password", "Memolito76");
        conn = DriverManager.getConnection("jdbc:mysql://dam-m06.cgvmbryjzif1.us-east-1.rds.amazonaws.com/test", connectionProps);
        return conn;
    }
}
